<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use function Safe\curl_init;
use function Safe\curl_exec;
use function Safe\curl_setopt;
use function Safe\json_decode;

class Vacunas
{
    private string $baseUrl = 'https://monitor.uruguaysevacuna.gub.uy/plugin/cda/api/doQuery?';

    private Carbon $minDate ;
    private Carbon $maxDate;

    private array $endpoints = [
        'sql_evolucion',
        'sql_evolucion_tabla',
        'sql_fechas_validas',
        'sql_filtro_centro_vacuna',
        'sql_filtro_dpto',
        'sql_filtro_dosis',
        'sql_filtro_edad_rango',
        'sql_filtro_localidad',
        'sql_filtro_poblacion',
        'sql_filtro_sexo',
        'sql_filtro_tipo_vacuna',
        'sql_indicadores_generales',
        'sql_indicadores_gral_agenda',
        'sql_vacunas_dpto_vacunatorio',
        'sql_vacunas_dpto_vacunatorio_escala',
        'sql_vacunas_poblacion',
        'sql_vacunas_rango_edad',
        'sql_vacunas_rango_edad_sexo',
        'sql_vacunas_tipo_vacuna',
        'sql_vacunas_tipo_vacuna_sexo',
    ];

    private array $defaultParameters;
    private array $headers;


    public function __construct()
    {
        $this->minDate = Carbon::parse("27-02-2021");
        $this->maxDate = now()->startOfDay();

        $this->defaultParameters = [
            'path' => '/public/Epidemiologia/Vacunas Covid/Paneles/Vacunas Covid/VacunasCovid.cda',
            'outputIndexId' => 1,
            'pageSize' => 0,
            'pageStart' => 0,
            'sortBy' => '',
            'paramSearchBox' => ''
        ];

        $this->headers = [
            'Accept:  */*',
            'Accept-Language:  es-UY,en;q=0.5',
            'Content-Type:  application/x-www-form-urlencoded;charset=UTF-8',
            'Origin:  https://monitor.uruguaysevacuna.gub.uy',
            'User-Agent:  Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
            'X-Requested-With:  XMLHttpRequest',
        ];
    }

    public function getHistoricalVaccinesByDepartment(?Carbon $start = null, Carbon $end = null)
    {
        if (empty($start)) {
            $start = $this->minDate;
        }
        if (empty($end)) {
            $end = $this->maxDate->subDay();
        }
        $query = [
            'dataAccessId' => 'sql_vacunas_depto_vacunatorio',
            'paramp_periodo_desde_sk' => $start->format('Ymd'),
            'paramp_periodo_hasta_sk' => $end->format('Ymd'),
            // 'paramp_ncto_desde_sk' => 0,    // ¯\_(ツ)_/¯
            // 'paramp_ncto_hasta_sk' => 0,    // ¯\_(ツ)_/¯
            'sortBy' => 2,  // Departamento
        ];

        return $this->doCall($query);
    }

    public function getHistoricalVaccinesByPopulationGroup(?Carbon $start = null, Carbon $end = null)
    {
        if (empty($start)) {
            $start = $this->minDate;
        }
        if (empty($end)) {
            $end = $this->maxDate->subDay();
        }
        $query = [
            'dataAccessId' => 'sql_vacunas_poblacion',
            'paramp_periodo_desde_sk' => $start->format('Ymd'),
            'paramp_periodo_hasta_sk' => $end->format('Ymd'),
        ];

        return $this->doCall($query);
    }

    public function getNumberOfVaccinationsAndCenters() : array
    {
        if (empty($start)) {
            $start = $this->minDate;
        }
        if (empty($end)) {
            $end = $this->maxDate->subDay();
        }
        $query = [
            'dataAccessId' => 'sql_indicadores_generales',
            // 'paramp_periodo_desde_sk' => $start->format('Ymd'),
            // 'paramp_periodo_hasta_sk' => $end->format('Ymd'),
        ];

        return $this->doCall($query);
    }

    public function getHistoricalVaccinesByAge(?Carbon $start = null, Carbon $end = null, int $rangoTipo = 6)
    {
        if (empty($start)) {
            $start = $this->minDate;
        }
        if (empty($end)) {
            $end = $this->maxDate->subDay();
        }
        $query = [
            'dataAccessId' => 'sql_vacunas_rango_edad',
            'paramp_periodo_desde_sk' => $start->format('Ymd'),
            'paramp_periodo_hasta_sk' => $end->format('Ymd'),
            'paramp_rango_tipo' => $rangoTipo,
        ];

        return $this->doCall($query);
    }

    private function doCall(array $query) : array
    {
        $ch = curl_init();

        $payload = http_build_query(array_merge($this->defaultParameters, $query));

        curl_setopt($ch, CURLOPT_URL, $this->baseUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $headers = $this->headers;
        $headers[] = 'Content-Length: ' . mb_strlen($payload);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close($ch);

        Log::debug("Payload: $payload");
        Log::debug("Response: $response");

        return json_decode($response, true);
    }
}
