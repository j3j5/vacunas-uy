<?php

namespace App\Processors;

use NumberFormatter;

class SqlVacunasDeptoVacunatorio
{
    protected $data;

    public function __construct(array $response)
    {
        $this->data = $response;
    }

    public function getProcessedData() : array
    {
        $data = collect($this->data['resultset'])->mapWithKeys(function ($dpto) {
            $fmt = new NumberFormatter('es_UY', NumberFormatter::TYPE_INT32);
            return [$dpto[2] => (int) $fmt->parse(trim($dpto[1]))];
        })->sortKeys();

        return $data->toArray();
    }
}
