<?php

namespace App\Processors;

class SqlVacunasPoblacion
{
    protected $data;

    public function __construct(array $response)
    {
        $this->data = $response;
    }

    public function getProcessedData() : array
    {
        $data = collect($this->data['resultset'])->mapWithKeys(function ($group) {
            return [$group[0] => $group[1]];
        })->sortKeys();

        return $data->toArray();
    }
}
