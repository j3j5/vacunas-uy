<?php

namespace App\Console\Commands;

use App\Processors\SqlVacunasDeptoVacunatorio;
use App\Processors\SqlVacunasPoblacion;
use App\Processors\SqlVacunasRangoEdad;
use App\Services\Vacunas;
use Exception;
use GrahamCampbell\GitHub\GitHubManager;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class UpdateGithubGist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacunas:gist
        {--start=2021-02-27}
        {--end=}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data to Github';

    protected Vacunas $api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Vacunas $api, GitHubManager $github) : int
    {
        $this->api = $api;
        $upload = [
            'files' => []
        ];
        // // Department
        // try {
        //     $dptoData = $this->getDataByDpto();
        //     $dosis1Depto = $this->getDosis1($dptoData);
        //     $upload['files']['vacunas-dpto-dosis1-historical.csv'] = [
        //         'content' => $this->arrayToCSVStr($dosis1Depto->toArray()),
        //     ];
        //     $dosis2Depto = $this->getDosis2($dptoData);
        //     $upload['files']['vacunas-dpto-dosis2-historical.csv'] = [
        //         'content' => $this->arrayToCSVStr($dosis2Depto->toArray()),
        //     ];
        //     $dosis1PercByDepto = $this->getDosis1Perc($dptoData);
        //     $upload['files']['vacunas-dpto-perc-dosis1-historical.csv'] = [
        //         'content' => $this->arrayToCSVStr($dosis1PercByDepto->toArray()),
        //     ];
        //     $dosis2PercByDepto = $this->getDosis2Perc($dptoData);
        //     $upload['files']['vacunas-dpto-perc-dosis2-historical.csv'] = [
        //         'content' => $this->arrayToCSVStr($dosis2PercByDepto ->toArray()),
        //     ];
        // } catch (Exception $e) {
        //     $this->error($e->getMessage());
        //     Log::error($e->getMessage());
        // }

        // Population
        try {
            $popGroupData = $this->getDataByPopulationGroup();
            $upload['files']['vacunas-poblacion-historical.csv'] = [
                'content' => $this->arrayToCSVStr($popGroupData->toArray())
            ];
        } catch (Exception $e) {
            $this->error($e->getMessage());
            Log::error($e->getMessage());
        }

        // Age
        try {
            $ageData = $this->getDataByAge();
            $upload['files']['vacunas-edad-historical.csv'] = [
                'content' => $this->arrayToCSVStr($ageData->toArray())
            ];
        } catch (Exception $e) {
            $this->error($e->getMessage());
            Log::error($e->getMessage());
        }

        try {
            $age2Data = $this->getDataByAge(true);
            $upload['files']['vacunas-edad2-historical.csv'] = [
                'content' => $this->arrayToCSVStr($age2Data->toArray())
            ];
        } catch (Exception $e) {
            $this->error($e->getMessage());
            Log::error($e->getMessage());
        }
        if (!empty($upload['files'])) {
            $github->gists()->update(config('github.gist'), $upload);
            $this->info('Check it out at https://gist.github.com/j3j5/' . config('github.gist'));
        } else {
            $this->error("No endpoints could be processed.");
            return -1;
        }

        return 0;
    }

    private function getDataByPopulationGroup() : Collection
    {
        $this->info("Getting data by population group");

        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        $data = collect();
        $csvHeaders = [];
        while ($start->lte($end)) {
            $response = $this->api->getHistoricalVaccinesByPopulationGroup($start, $start);
            if (is_array($response)) {
                $processor = new SqlVacunasPoblacion($response);
                $apiData = $processor->getProcessedData();
                $csvHeaders = $this->fillHeaders($apiData, $csvHeaders);
                $data->put($start->format('Y-m-d'), $apiData);
            }

            $start->addDay();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $data = $data->map(function ($row, $date) use ($csvHeaders) {
            foreach ($csvHeaders as $header => $val) {
                if (!isset($row[$header])) {
                    $row[$header] = 0;
                }
            }
            ksort($row);
            return Arr::prepend(array_values($row), $date);
        });

        $data->prepend(Arr::prepend(array_keys($csvHeaders), ''));
        return $data;
    }

    private function getDataByAge($detailed = false) : Collection
    {
        $msg = "Getting data by age";
        $msg .= $detailed ? ' (detailed)' : '';
        $this->info($msg);

        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        $data = collect();
        $csvHeaders = [];
        while ($start->lte($end)) {
            $tipo = $detailed ? 7 : 6;
            $response = $this->api->getHistoricalVaccinesByAge($start, $start, $tipo);
            if (is_array($response)) {
                $processor = new SqlVacunasRangoEdad($response);
                $apiData = $processor->getProcessedData();
                $csvHeaders = $this->fillHeaders($apiData, $csvHeaders);
                $data->put($start->format('Y-m-d'), $apiData);
            }

            $start->addDay();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $data = $data->map(function ($row, $date) use ($csvHeaders) {
            foreach ($csvHeaders as $header => $val) {
                if (!isset($row[$header])) {
                    $row[$header] = 0;
                }
            }
            ksort($row, SORT_NATURAL);
            return Arr::prepend(array_values($row), $date);
        });

        $data->prepend(Arr::prepend(array_keys($csvHeaders), ''));

        return $data;
    }

    private function fillHeaders(array $data, array $headers) : array
    {
        $newHeaders = array_keys($data);
        foreach ($newHeaders as $header) {
            if (isset($headers[$header])) {
                continue;
            }
            $headers[$header] = true;
        }

        ksort($headers, SORT_NATURAL);

        return $headers;
    }


    private function getDataByDpto() : Collection
    {
        $this->info("Getting data by department");
        $dptoData = collect();
        $csvHeaders = [];
        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        while ($start->lte($end)) {
            $response = $this->api->getHistoricalVaccinesByDepartment($start, $start);
            if (is_array($response)) {
                $processor = new SqlVacunasDeptoVacunatorio($response);
                $dayData = $processor->getProcessedData();
                $dptoData->put($start->format('Y-m-d'), $dayData);
            }

            $start->addDay();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        return $dptoData;
    }

    private function getDosis1(Collection $data) : Collection
    {
        $processedData = $data->map(function ($series, $date) {
            $rows =  collect($series)->map(function (array $dptoData, string $dpto) {
                return $dptoData['Dosis 1'];
            })->values();
            return $rows->prepend($date)->toArray();
        });

        $headers = Arr::prepend(array_keys($data->first()), '');
        $processedData = $processedData->prepend($headers)->values();
        return $processedData;
    }

    private function getDosis2(Collection $data) : Collection
    {
        $processedData = $data->map(function ($series, $date) {
            $rows =  collect($series)->map(function (array $dptoData, string $dpto) {
                return $dptoData['Dosis 2'];
            })->values();
            return $rows->prepend($date)->toArray();
        });

        $headers = Arr::prepend(array_keys($data->first()), '');
        return $processedData->prepend($headers)->values();
    }

    private function getDosis1Perc(Collection $data) : Collection
    {
        $processedData = $data->map(function ($series, $date) {
            $rows =  collect($series)->map(function (array $dptoData, string $dpto) {
                return $dptoData['% con Dosis 1'];
            })->values();
            return $rows->prepend($date)->toArray();
        });

        $headers = Arr::prepend(array_keys($data->first()), '');
        return $processedData->prepend($headers)->values();
    }

    private function getDosis2Perc(Collection $data) : Collection
    {
        $processedData = $data->map(function ($series, $date) {
            $rows =  collect($series)->map(function (array $dptoData, string $dpto) {
                return $dptoData['% con Dosis 2'];
            })->values();
            return $rows->prepend($date)->toArray();
        });

        $headers = Arr::prepend(array_keys($data->first()), '');
        return $processedData->prepend($headers)->values();
    }

    /**
     * Convert a multi-dimensional, associative array to CSV data
     * @param  array $data the array of data
     * @return string       CSV text
     */
    private function arrayToCSVStr($data) : string
    {
        // Generate CSV data from array
        $fh = fopen('php://temp', 'rw'); // don't create a file, attempt
                                        // to use memory instead
        // write out the data
        foreach ($data as $row) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        rewind($fh);
        fclose($fh);

        return $csv;
    }
}
