<?php

namespace App\Console\Commands;

use App\Processors\SqlVacunasPoblacion;
use App\Services\Vacunas;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use function Safe\fopen;
use function Safe\fclose;
use function Safe\fputcsv;

class DownloadHistoricalDataByPopulationGroup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacunas:poblacion
        {--start=2021-02-27}
        {--end=}
        {--filename=}
        {--show-only}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Descarga el histórico de vacunaciones por grupo de población';

    protected array $csvHeaders;
    /**
     * file pointer
     * @var mixed
     */
    protected $fp;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Vacunas $api) : int
    {
        $data = [];
        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        $filename = $this->option('filename') ?? 'vacunas-poblacion-historical-' . now()->format('Y-m-d') . '.csv';
        $data = collect();
        while ($start->lte($end)) {
            $response = $api->getHistoricalVaccinesByPopulationGroup($start, $start);
            if (is_array($response)) {
                $processor = new SqlVacunasPoblacion($response);
                $apiData = $processor->getProcessedData();
                if (
                    empty($this->csvHeaders) ||
                    count($apiData) > count($this->csvHeaders)
                ) {
                    $this->csvHeaders = array_keys($apiData);
                }
                $data->put($start->format('Y-m-d'), $apiData);
            }

            $start->addDay();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        $data = $data->mapWithKeys(function ($row, $date) {
            foreach ($this->csvHeaders as $header) {
                if (!isset($row[$header])) {
                    $row[$header] = 0;
                }
            }
            ksort($row);
            return [$date => $row];
        });

        if (!$this->option('show-only')) {
            $this->fp = fopen($filename, 'w');
            fputcsv($this->fp, Arr::prepend($this->csvHeaders, ''));
            $data->each(function ($row, $date) {
                fputcsv($this->fp, Arr::prepend($row, $date));
            });
            fclose($this->fp);
        } else {
            dump($this->csvHeaders, $data);
            // $this->table(array_keys($data), [array_values($data)]);
        }


        return 0;
    }
}
