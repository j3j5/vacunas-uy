<?php

namespace App\Console\Commands;

use App\Processors\SqlVacunasDeptoVacunatorio;
use App\Services\Vacunas;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use function Safe\fopen;
use function Safe\fclose;
use function Safe\fputcsv;

class DownloadHistoricalDataByDepartment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacunas:dpto
        {--start=2021-02-27}
        {--end=}
        {--filename=}
        {--show-only}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Descarga el histórico de vacunaciones por departamento';

    protected array $csvHeaders;
    /**
     * file pointer
     * @var mixed
     */
    protected $fp;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Vacunas $api) : int
    {
        $data = [];
        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        $filename = $this->option('filename') ?? 'vacunas-dpto-historical-' . now()->format('Y-m-d') . '.csv';
        if (!$this->option('show-only')) {
            $this->fp = fopen($filename, 'w');
        }

        while ($start->lte($end)) {
            $response = $api->getHistoricalVaccinesByDepartment($start, $start);
            if (is_array($response)) {
                $processor = new SqlVacunasDeptoVacunatorio($response);
                $data = $processor->getProcessedData();
                if (!$this->option('show-only')) {
                    if (empty($this->csvHeaders)) {
                        $this->csvHeaders = Arr::prepend(array_keys($data), '');
                        fputcsv($this->fp, $this->csvHeaders);
                    }
                    fputcsv($this->fp, Arr::prepend(array_values($data), $start->format('Y-m-d')));
                } else {
                    $this->table(array_keys($data), [array_values($data)]);
                }
            }

            $start->addDay();
            $this->output->progressAdvance();
        }
        fclose($this->fp);

        $this->output->progressFinish();

        return 0;
    }
}
