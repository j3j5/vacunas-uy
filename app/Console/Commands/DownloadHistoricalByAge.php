<?php

namespace App\Console\Commands;

use App\Processors\SqlVacunasRangoEdad;
use App\Services\Vacunas;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class DownloadHistoricalByAge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacunas:edad
        {--start=2021-02-27}
        {--end=}
        {--filename=}
        {--show-only}
        {--detailed}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Descargar serie histórica por edad';

    private array $csvHeaders = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Vacunas $api)
    {
        $start = Carbon::parse($this->option('start'));
        $end = Carbon::parse($this->option('end') ?? now()->format('Y-m-d'));
        $this->output->progressStart($end->diffInDays($start) + 1);

        $defaultFilename = 'vacunas-age-historical-' . now()->format('Y-m-d');
        $defaultFilename .= $this->option('detailed') ? '-detailed'  : '';
        $defaultFilename .= '.csv';

        $filename = $this->option('filename') ?? $defaultFilename;
        if (!$this->option('show-only')) {
            $this->fp = fopen($filename, 'w');
        }

        $data = collect();
        while ($start->lte($end)) {
            $tipo = $this->option('detailed') ? 7 : 6;
            $response = $api->getHistoricalVaccinesByAge($start, $start, $tipo);
            if (is_array($response)) {
                $processor = new SqlVacunasRangoEdad($response);
                $apiData = $processor->getProcessedData();
                $this->fillHeaders($apiData);
                $data->put($start->format('Y-m-d'), $apiData);
            }

            $start->addDay();
            $this->output->progressAdvance();
        }

        $data = $data->map(function ($row, $date) {
            foreach ($this->csvHeaders as $header => $val) {
                if (!isset($row[$header])) {
                    $row[$header] = 0;
                }
            }
            ksort($row, SORT_NATURAL);
            return Arr::prepend(array_values($row), $date);
        });

        $this->output->progressFinish();

        if ($this->option('show-only')) {
            $this->table(Arr::prepend($this->csvHeaders, ''), $data->toArray());
        } else {
            fputcsv($this->fp, Arr::prepend(array_keys($this->csvHeaders), ''));
            $data->each(function ($row) {
                fputcsv($this->fp, $row);
            });
            fclose($this->fp);
        }

        return 0;
    }

    private function fillHeaders(array $data)
    {
        $headers = array_keys($data);
        foreach ($headers as $header) {
            if (isset($this->csvHeaders[$header])) {
                continue;
            }
            $this->csvHeaders[$header] = true;
        }

        ksort($this->csvHeaders, SORT_NATURAL);
    }
}
